package com.mriso.oximetro.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.material.tabs.TabLayout;
import com.mriso.oximetro.adapter.Adapter_PagerMain;
import com.mriso.oximetro.controller.ControllerDatos;
import com.mriso.oximetro.R;
import com.mriso.oximetro.fragments.ListaPacientes_fragment;
import com.mriso.oximetro.fragments.Mediciones_Fragment;
import com.mriso.oximetro.model.Paciente;

import pl.tajchert.nammu.Nammu;

public class Main_Activity extends AppCompatActivity implements ListaPacientes_fragment.Graficable{

    private ViewPager pager;
    private Adapter_PagerMain adapterPager;
    private TabLayout tabLayout;
    private ControllerDatos controllerDatos;
    private SharedPreferences config;
    private String idUser;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



///     --SETEAR ACTION BAR
        ActionBar actionBar = getSupportActionBar();
        assert actionBar != null;
        actionBar.setBackgroundDrawable(getResources().getDrawable(R.drawable.actionbar));
///     SETEAR ACTION BAR--

//      --INICIAR NAMU
        Nammu.init(getApplicationContext());
//      INICIAR NAMMU--

        controllerDatos=new ControllerDatos(this);

//       ---ME FIJO SI ES LA PRIMERA VEZ QUE CORRE Y POPULO DB
        config = this.getSharedPreferences("prefs", 0);
        boolean firstRun = config.getBoolean("firstRun", false);
        if (!firstRun){
            controllerDatos.cargarBaseDeDatos();
            SharedPreferences.Editor editor = config.edit();
            editor.putBoolean("firstRun", true);
            editor.commit();
        }
//        ME FIJO SI ES LA PRIMERA VEZ QUE CORRE Y POPULO DB---

        pager= findViewById(R.id.viewPagerMyAudits);
        adapterPager= new Adapter_PagerMain(getSupportFragmentManager(),controllerDatos.traerListaViewPagerMain());
        pager.setAdapter(adapterPager);
        adapterPager.notifyDataSetChanged();

        TabLayout tabLayout = findViewById(R.id.tabLayoutMain);
        tabLayout.setupWithViewPager(pager);
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.nav_menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        int elid=item.getItemId();
        switch (elid){

            case R.id.botonSalir:
                preguntarSalir();
            break;
        }

        return true;
    }

    private void preguntarSalir() {

        new MaterialDialog.Builder(Main_Activity.this)
                .contentColor(ContextCompat.getColor(Main_Activity.this, R.color.turquesaoscuro))
                .titleColor(ContextCompat.getColor(Main_Activity.this, R.color.oscuro))
                .title(R.string.quit)
                .content(R.string.des_quit)
                .positiveText(R.string.quit)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                       Main_Activity.this.finishAffinity();
                    }
                })
                .negativeText(R.string.cancel)
                .show();
    }

    private void preguntarCierreSesion() {
        new MaterialDialog.Builder(Main_Activity.this)
                .contentColor(ContextCompat.getColor(Main_Activity.this, R.color.turquesaoscuro))
                .titleColor(ContextCompat.getColor(Main_Activity.this, R.color.oscuro))
                .title(R.string.quit)
                .content(R.string.des_quit)
                .positiveText(R.string.quit)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                       irLogin();
                    }
                })
                .negativeText(R.string.cancel)
                .show();
    }

    private void irLogin() {
        SharedPreferences.Editor editor = config.edit();
        editor.putBoolean("logged", false);
        editor.commit();

        Intent mainIntent = new Intent(Main_Activity.this,Login_Activity.class);
        Main_Activity.this.startActivity(mainIntent);
        Main_Activity.this.finish();

    }

    @Override
    public void onBackPressed() {
       preguntarSalir();
    }


    @Override
    public void GraficarDatosPaciente(Paciente unPacienteGraficable) {
        Intent elIntent = new Intent(Main_Activity.this,Grafico_Activity.class);
        Bundle bundle= new Bundle();
        bundle.putString(Grafico_Activity.IDPACIENTE,unPacienteGraficable.getDocumento());
        elIntent.putExtras(bundle);
        Main_Activity.this.startActivity(elIntent);
    }
}

package com.mriso.oximetro.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.mriso.oximetro.R;
import com.mriso.oximetro.model.Paciente;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;


/**
 * A simple {@link Fragment} subclass.
 */
public class TerceraOpcion_Fragment extends Fragment {



    public TerceraOpcion_Fragment() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.tercer_fragment, container, false);




        return view;
    }
    public static TerceraOpcion_Fragment createSegundaOpcionFragment(){
        return new TerceraOpcion_Fragment();
    }

}

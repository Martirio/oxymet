package com.mriso.oximetro.fragments;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mriso.oximetro.R;
import com.mriso.oximetro.adapter.Adapter_Mediciones_Main;
import com.mriso.oximetro.model.Medicion;
import com.mriso.oximetro.model.Paciente;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;
import io.realm.Sort;


/**
 * A simple {@link Fragment} subclass.
 */
public class Mediciones_Fragment extends Fragment {



    public Mediciones_Fragment() {
        // Required empty public constructor
    }




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_mediciones, container, false);

        Realm realm = Realm.getDefaultInstance();

        RealmResults<Medicion> listaMedicionesRealm = realm.where(Medicion.class)
                .sort("timestamp", Sort.ASCENDING)
                .findAll();
        RealmList<Medicion> listaMediciones = new RealmList<>();
        listaMediciones.addAll(listaMedicionesRealm);
//      BUSCAR EN REALM LISTA DE PACIENTES(TODOS LOS PACIENTES CORRESPONDEN AL MISMO GESTOR)---

//      ---RECYCLERVIEW SETTING
        final RecyclerView recyclerMisPacientes = view.findViewById(R.id.recyclerMisPacientes);
        final Adapter_Mediciones_Main adapterPacientes = new Adapter_Mediciones_Main();
        adapterPacientes.setContext(getContext());
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        recyclerMisPacientes.setLayoutManager(layoutManager);
        adapterPacientes.setListaMedicionesOriginales(listaMediciones);
        recyclerMisPacientes.setAdapter(adapterPacientes);

//      RECYCLERVIEW SETTING---

        return view;
    }
    public static Mediciones_Fragment createMedicionesFragment(){
        return new Mediciones_Fragment();
    }

}

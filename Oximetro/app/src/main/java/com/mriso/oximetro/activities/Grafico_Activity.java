package com.mriso.oximetro.activities;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.google.android.material.tabs.TabLayout;
import com.mriso.oximetro.R;
import com.mriso.oximetro.adapter.Adapter_PagerMain;
import com.mriso.oximetro.controller.ControllerDatos;
import com.mriso.oximetro.fragments.Grafico_Fragment;
import com.mriso.oximetro.model.Medicion;
import com.mriso.oximetro.model.Paciente;

import java.util.ArrayList;
import java.util.Random;

import io.realm.Realm;
import io.realm.RealmResults;
import ir.androidexception.datatable.DataTable;
import ir.androidexception.datatable.model.DataTableHeader;
import ir.androidexception.datatable.model.DataTableRow;
import pl.tajchert.nammu.Nammu;

public class Grafico_Activity extends AppCompatActivity {

    public static final String IDPACIENTE="IDPACIENTE";
    private String idPaciente;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grafico);

        Intent elIntent= getIntent();
        Bundle bundle=elIntent.getExtras();
        if (bundle!=null){
            idPaciente=bundle.getString(IDPACIENTE);
        }
///     --SETEAR ACTION BAR
        ActionBar actionBar = getSupportActionBar();
        assert actionBar != null;
        actionBar.setBackgroundDrawable(getResources().getDrawable(R.drawable.actionbar));
        actionBar.setDisplayHomeAsUpEnabled(true);

///     SETEAR ACTION BAR--

        cargarFragmentGraficos(idPaciente);






    }



    private void cargarFragmentGraficos(String idPaciente) {

        Grafico_Fragment fragmentGrafico = new Grafico_Fragment();
        Bundle bundle = new Bundle();
        bundle.putString(Grafico_Fragment.IDPACIENTE,idPaciente);
        fragmentGrafico.setArguments(bundle);
        FragmentManager fragmentManager= getSupportFragmentManager();
        FragmentTransaction fragmentTransaction=fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.contenedorGraficos,fragmentGrafico,"grafico");
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    @Override
    public void onBackPressed() {
        Grafico_Activity.this.finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
}

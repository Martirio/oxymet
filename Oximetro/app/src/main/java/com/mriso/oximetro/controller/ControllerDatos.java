package com.mriso.oximetro.controller;

import android.content.Context;


import com.mriso.oximetro.R;
import com.mriso.oximetro.model.Gestor;
import com.mriso.oximetro.model.Medicion;
import com.mriso.oximetro.model.Paciente;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import de.siegmar.fastcsv.reader.CsvContainer;
import de.siegmar.fastcsv.reader.CsvReader;
import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;


/**
 * Created by elmar on 13/8/2017.
 */

public class ControllerDatos {

    private Context context;

    public ControllerDatos(Context context) {
        this.context = context;
    }


    public List<String> traerListaViewPagerMain(){
        List<String> unaLista=new ArrayList<>();
        unaLista.add("Pacientes");
        unaLista.add("Mediciones");
        unaLista.add("Opciones");
        return unaLista;
    }

    public void cargarBaseDeDatos(){

        Realm realm = Realm.getDefaultInstance();
        CsvReader reader= new CsvReader();
        reader.setContainsHeader(true);
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {

                RealmList<Medicion> listaMediciones= new RealmList<>();
                listaMediciones.add(cargarMedicion("9016513", "1581621753","83","110","367","90"));
                listaMediciones.add(cargarMedicion("9016513", "1582380636","77","105","366","14"));
                listaMediciones.add(cargarMedicion("9016513", "1580119242","82","108","375","65"));
                listaMediciones.add(cargarMedicion("9016513", "1585519953","92","63","369","83"));
                listaMediciones.add(cargarMedicion("9016513", "1580578960","85","107","368","44"));
                listaMediciones.add(cargarMedicion("9016513", "1579249125","82","106","373","85"));
                listaMediciones.add(cargarMedicion("9016513", "1579697602","94","70","372","93"));
                listaMediciones.add(cargarMedicion("9016513", "1584817131","93","71","374","8"));
                listaMediciones.add(cargarMedicion("9016513", "1580776041","86","92","370","76"));
                listaMediciones.add(cargarMedicion("9016513", "1581870891","92","63","380","51"));
                listaMediciones.add(cargarMedicion("24357024","1579564415","81","112","390","4"));
                listaMediciones.add(cargarMedicion("24357024","1585484460","86","115","369","14"));
                listaMediciones.add(cargarMedicion("24357024","1584654279","79","101","380","70"));
                listaMediciones.add(cargarMedicion("24357024","1583615996","89","67","362","53"));
                listaMediciones.add(cargarMedicion("24357024","1578020461","86","74","362","58"));
                listaMediciones.add(cargarMedicion("24357024","1579014091","85","120","365","74"));
                listaMediciones.add(cargarMedicion("24357024","1579578383","91","79","361","24"));
                listaMediciones.add(cargarMedicion("24357024","1583199396","95","108","371","60"));
                listaMediciones.add(cargarMedicion("24357024","1584159094","95","109","364","65"));
                listaMediciones.add(cargarMedicion("24357024","1580497732","83","96","366","74"));
                listaMediciones.add(cargarMedicion("14682961","1580230983","94","106","366","32"));
                listaMediciones.add(cargarMedicion("14682961","1584772977","79","120","373","43"));
                listaMediciones.add(cargarMedicion("14682961","1585487558","81","89","380","35"));
                listaMediciones.add(cargarMedicion("14682961","1583556576","85","81","363","43"));
                listaMediciones.add(cargarMedicion("14682961","1581138083","83","113","370","90"));
                listaMediciones.add(cargarMedicion("14682961","1577903658","76","87","371","5"));
                listaMediciones.add(cargarMedicion("14682961","1580692095","78","113","373","7"));
                listaMediciones.add(cargarMedicion("14682961","1584603753","94","99","371","23"));
                listaMediciones.add(cargarMedicion("14682961","1584712549","86","93","375","39"));
                listaMediciones.add(cargarMedicion("14682961","1582137146","79","100","390","37"));
                listaMediciones.add(cargarMedicion("13583559","1578011718","76","118","370","63"));
                listaMediciones.add(cargarMedicion("13583559","1582610929","91","106","369","81"));
                listaMediciones.add(cargarMedicion("13583559","1582188524","91","63","371","67"));
                listaMediciones.add(cargarMedicion("13583559","1582623842","76","111","366","19"));
                listaMediciones.add(cargarMedicion("13583559","1585553026","86","79","364","81"));
                listaMediciones.add(cargarMedicion("13583559","1581811316","85","66","370","49"));
                listaMediciones.add(cargarMedicion("13583559","1584176041","83","92","374","11"));
                listaMediciones.add(cargarMedicion("13583559","1583782147","80","69","374","44"));
                listaMediciones.add(cargarMedicion("13583559","1580881213","91","93","363","27"));
                listaMediciones.add(cargarMedicion("13583559","1583132879","90","68","372","51"));
                listaMediciones.add(cargarMedicion("6090495", "1580803926","87","65","366","48"));
                listaMediciones.add(cargarMedicion("6090495", "1581201130","83","81","374","93"));
                listaMediciones.add(cargarMedicion("6090495", "1581443784","92","70","364","37"));
                listaMediciones.add(cargarMedicion("6090495", "1581489966","89","80","374","9"));
                listaMediciones.add(cargarMedicion("6090495", "1578441282","90","104","371","21"));
                listaMediciones.add(cargarMedicion("6090495", "1578281847","76","98","369","68"));
                listaMediciones.add(cargarMedicion("6090495", "1584381610","82","83","380","63"));
                listaMediciones.add(cargarMedicion("6090495", "1580295292","82","95","370","76"));
                listaMediciones.add(cargarMedicion("6090495", "1580818843","87","103","365","69"));
                listaMediciones.add(cargarMedicion("6090495", "1579775541","88","115","372","83"));
                listaMediciones.add(cargarMedicion("16062723","1585628342","80","76","373","9"));
                listaMediciones.add(cargarMedicion("16062723","1583824865","81","120","368","80"));
                listaMediciones.add(cargarMedicion("16062723","1579123495","85","61","380","9"));
                listaMediciones.add(cargarMedicion("16062723","1581362463","88","102","367","90"));
                listaMediciones.add(cargarMedicion("16062723","1582863156","92","119","385","48"));
                listaMediciones.add(cargarMedicion("16062723","1581761548","84","102","363","54"));
                listaMediciones.add(cargarMedicion("16062723","1580696079","79","89","366","29"));
                listaMediciones.add(cargarMedicion("16062723","1581056602","84","62","372","91"));
                listaMediciones.add(cargarMedicion("16062723","1578498711","91","100","364","18"));
                listaMediciones.add(cargarMedicion("16062723","1580422586","75","77","363","59"));
                listaMediciones.add(cargarMedicion("15244623","1580847538","92","74","366","1"));
                listaMediciones.add(cargarMedicion("15244623","1580244129","89","120","361","32"));
                listaMediciones.add(cargarMedicion("15244623","1584950422","76","110","374","63"));
                listaMediciones.add(cargarMedicion("15244623","1581007364","91","79","366","19"));
                listaMediciones.add(cargarMedicion("15244623","1583728772","80","103","380","5"));
                listaMediciones.add(cargarMedicion("15244623","1584677083","82","60","375","38"));
                listaMediciones.add(cargarMedicion("15244623","1581431029","84","111","374","42"));
                listaMediciones.add(cargarMedicion("15244623","1583464668","77","76","371","76"));
                listaMediciones.add(cargarMedicion("15244623","1582804247","79","73","363","85"));
                listaMediciones.add(cargarMedicion("15244623","1577913761","87","82","372","86"));
                listaMediciones.add(cargarMedicion("28217039","1578217285","94","87","371","85"));
                listaMediciones.add(cargarMedicion("28217039","1584144276","88","67","364","33"));
                listaMediciones.add(cargarMedicion("28217039","1581941527","83","117","364","85"));
                listaMediciones.add(cargarMedicion("28217039","1580982424","87","120","369","80"));
                listaMediciones.add(cargarMedicion("28217039","1585191236","85","79","370","37"));
                listaMediciones.add(cargarMedicion("28217039","1584422518","84","112","371","83"));
                listaMediciones.add(cargarMedicion("28217039","1582546999","91","72","367","79"));
                listaMediciones.add(cargarMedicion("28217039","1579866198","76","91","362","69"));
                listaMediciones.add(cargarMedicion("28217039","1583001604","92","118","363","85"));
                listaMediciones.add(cargarMedicion("28217039","1584378774","80","92","367","37"));
                listaMediciones.add(cargarMedicion("13354088","1578094736","76","119","367","6"));
                listaMediciones.add(cargarMedicion("13354088","1578335659","78","111","362","33"));
                listaMediciones.add(cargarMedicion("13354088","1582483504","93","94","367","76"));
                listaMediciones.add(cargarMedicion("13354088","1581184901","76","71","370","79"));
                listaMediciones.add(cargarMedicion("13354088","1583828209","92","77","375","9"));
                listaMediciones.add(cargarMedicion("13354088","1580101049","84","106","375","48"));
                listaMediciones.add(cargarMedicion("13354088","1578191463","92","119","375","0"));
                listaMediciones.add(cargarMedicion("13354088","1584629137","77","95","370","11"));
                listaMediciones.add(cargarMedicion("13354088","1578500974","77","107","368","56"));
                listaMediciones.add(cargarMedicion("13354088","1580184837","92","102","366","25"));
                listaMediciones.add(cargarMedicion("11568626","1582804896","92","69","373","42"));
                listaMediciones.add(cargarMedicion("11568626","1578542805","79","62","361","19"));
                listaMediciones.add(cargarMedicion("11568626","1583617062","90","113","374","31"));
                listaMediciones.add(cargarMedicion("11568626","1581541056","83","101","361","11"));
                listaMediciones.add(cargarMedicion("11568626","1579111723","75","83","361","15"));
                listaMediciones.add(cargarMedicion("11568626","1582089941","85","60","374","59"));
                listaMediciones.add(cargarMedicion("11568626","1585676804","79","101","362","28"));
                listaMediciones.add(cargarMedicion("11568626","1579491174","83","94","368","79"));
                listaMediciones.add(cargarMedicion("11568626","1584084175","87","109","369","46"));
                listaMediciones.add(cargarMedicion("11568626","1579015710","81","101","390","58"));
                listaMediciones.add(cargarMedicion("15057478","1584029414","92","128"	,"381","10"));
                listaMediciones.add(cargarMedicion("15057478","1577929782","81","129"	,"379","70"));
                listaMediciones.add(cargarMedicion("15057478","1583512379","94","85","369","46"));
                listaMediciones.add(cargarMedicion("15057478","1579885490","80","108"	,"375","35"));
                listaMediciones.add(cargarMedicion("15057478","1582506891","92","82","384","64"));
                listaMediciones.add(cargarMedicion("15057478","1585044933","90","91","384","38"));
                listaMediciones.add(cargarMedicion("15057478","1579759485","75","127","362","35"));
                listaMediciones.add(cargarMedicion("15057478","1582968866","75","124","368","30"));
                listaMediciones.add(cargarMedicion("15057478","1582517548","84","115","366","71"));
                listaMediciones.add(cargarMedicion("15057478","1583480038","91","106"	,"374","71"));
                listaMediciones.add(cargarMedicion("12485737","1583232234","89","79","388","70"));
                listaMediciones.add(cargarMedicion("12485737","1582845345","83","85","366","51"));
                listaMediciones.add(cargarMedicion("12485737","1580186942","80","76","388","59"));
                listaMediciones.add(cargarMedicion("12485737","1578225648","87","127"	,"387","31"));
                listaMediciones.add(cargarMedicion("12485737","1579704011","80","100"	,"364","70"));
                listaMediciones.add(cargarMedicion("12485737","1579624796","89","84","376","55"));
                listaMediciones.add(cargarMedicion("12485737","1582119525","84","113"	,"383","15"));
                listaMediciones.add(cargarMedicion("12485737","1578362860","84","126"	,"365","24"));
                listaMediciones.add(cargarMedicion("17727254","1581617821","75","116"	,"372","38"));
                listaMediciones.add(cargarMedicion("17727254","1583575199","94","68","364","72"));
                listaMediciones.add(cargarMedicion("17727254","1584030385","85","92","381","63"));
                listaMediciones.add(cargarMedicion("17727254","1583775319","85","67","388","54"));
                listaMediciones.add(cargarMedicion("17727254","1578199922","90","130"	,"369","72"));
                listaMediciones.add(cargarMedicion("17727254","1583299436","77","121"	,"385","53"));
                listaMediciones.add(cargarMedicion("17727254","1579047017","82","74","388","69"));
                listaMediciones.add(cargarMedicion("17727254","1578175656","82","68","378","49"));
                listaMediciones.add(cargarMedicion("12425288","1582330259","76","82","390","75"));
                listaMediciones.add(cargarMedicion("12425288","1578411487","84","127"	,"387","66"));
                listaMediciones.add(cargarMedicion("12425288","1584786092","85","108"	,"386","15"));
                listaMediciones.add(cargarMedicion("12425288","1584099552","86","119"	,"364","70"));
                listaMediciones.add(cargarMedicion("12425288","1584961732","75","129"	,"382","13"));
                listaMediciones.add(cargarMedicion("22921135","1582461318","86","122"	,"360","55"));
                listaMediciones.add(cargarMedicion("22921135","1581204957","76","73","376","16"));
                listaMediciones.add(cargarMedicion("22921135","1581474524","78","65","372","75"));
                listaMediciones.add(cargarMedicion("22921135","1580037572","80","89","386","40"));
                listaMediciones.add(cargarMedicion("22921135","1584052155","91","124"	,"363","56"));
                listaMediciones.add(cargarMedicion("22921135","1582868873","75","82","366","61"));
                listaMediciones.add(cargarMedicion("22921135","1582532508","89","129"	,"376","80"));
                listaMediciones.add(cargarMedicion("11998937","1585578952","94","130"	,"386","21"));
                listaMediciones.add(cargarMedicion("11998937","1585673825","83","97","387","14"));
                listaMediciones.add(cargarMedicion("11998937","1579541099","92","107"	,"365","74"));
                listaMediciones.add(cargarMedicion("11998937","1583853361","87","120"	,"384","70"));
                listaMediciones.add(cargarMedicion("11998937","1585455587","88","92","362","49"));
                listaMediciones.add(cargarMedicion("16777740","1580447188","75","90","377","48"));
                listaMediciones.add(cargarMedicion("16777740","1584108445","95","99","383","19"));
                listaMediciones.add(cargarMedicion("16777740","1580461433","77","77","367","28"));
                listaMediciones.add(cargarMedicion("16777740","1582760001","93","98","365","30"));
                listaMediciones.add(cargarMedicion("25219800","1580913307","83","123"	,"368","84"));
                listaMediciones.add(cargarMedicion("25219800","1584786818","82","93","372","73"));
                listaMediciones.add(cargarMedicion("25219800","1581696376","93","67","382","53"));
                listaMediciones.add(cargarMedicion("25219800","1585505303","91","119"	,"366","16"));
                listaMediciones.add(cargarMedicion("25219800","1582682129","81","124"	,"381","41"));
                listaMediciones.add(cargarMedicion("16055126","1578349776","75","73","373","68"));
                listaMediciones.add(cargarMedicion("16055126","1581117801","90","73","366","23"));
                listaMediciones.add(cargarMedicion("16055126","1580816036","83","107"	,"360","55"));
                listaMediciones.add(cargarMedicion("16055126","1580529491","85","117"	,"378","74"));
                listaMediciones.add(cargarMedicion("16055126","1584656840","91","92","390","25"));
                listaMediciones.add(cargarMedicion("8422385", "1585045014","88","115","375","68"));
                listaMediciones.add(cargarMedicion("8422385", "1585054676","89","85","370","70"));
                listaMediciones.add(cargarMedicion("8422385", "1582671779","87","71","375","61"));
                listaMediciones.add(cargarMedicion("8422385", "1584964948","76","112","368","40"));
                listaMediciones.add(cargarMedicion("8422385", "1585422076","89","70","378","66"));
                listaMediciones.add(cargarMedicion("8422385", "1582211441","93","115","390","15"));
               
               
               
               
               
               

                for (Medicion laMed :
                        listaMediciones) {
                    realm.copyToRealmOrUpdate(laMed);
                }

                RealmList<Paciente> listaPacientes= new RealmList<>();
                listaPacientes.add(cargarPersona("Carlos Martinez","9016513"));
                listaPacientes.add(cargarPersona("Roberto Gomez","24357024"));
                listaPacientes.add(cargarPersona("Matias Garcia","14682961"));
                listaPacientes.add(cargarPersona("Walter Solis","13583559"));
                listaPacientes.add(cargarPersona("Lucas Perez","6090495"));
                listaPacientes.add(cargarPersona("Martin Sanchez","16062723"));
                listaPacientes.add(cargarPersona("Yamila Ramirez","15244623"));
                listaPacientes.add(cargarPersona("Silvina Torres","28217039"));
                listaPacientes.add(cargarPersona("Marina Flores","13354088"));
                listaPacientes.add(cargarPersona("Sonia Rivera","11568626"));
                listaPacientes.add(cargarPersona("Cecilia Gomez","15057478"));
                listaPacientes.add(cargarPersona("Paula Diaz","12485737"));
                listaPacientes.add(cargarPersona("Silvana Reyes","17727254"));
                listaPacientes.add(cargarPersona("Vanesa Cruz","12425288"));
                listaPacientes.add(cargarPersona("Hector Morales","22921135"));
                listaPacientes.add(cargarPersona("Eduardo Ortiz","11998937"));
                listaPacientes.add(cargarPersona("Federico Gutierrez","16777740"));
                listaPacientes.add(cargarPersona("Fernando Alemano","25219800"));
                listaPacientes.add(cargarPersona("Gerardo Crevie","16055126"));
                listaPacientes.add(cargarPersona("Pablo Solente","8422385"));

                Gestor elGestor = new Gestor("mn1234");
                Gestor realmGestor = realm.copyToRealmOrUpdate(elGestor);
                for (Paciente elPA :
                        listaPacientes) {
                    Paciente realmPaciente = realm.copyToRealmOrUpdate(elPA);
                    realmGestor.getListaPacientes().add(realmPaciente);
                    RealmResults<Medicion> medicionesRealm = realm.where(Medicion.class)
                            .equalTo("idPacienteRelacionado",elPA.getDocumento())
                            .findAll();
                    if (medicionesRealm!=null){
                        realmPaciente.getListaMediciones().addAll(medicionesRealm);
                    }
                }
            }
        });
    }

    private Paciente cargarPersona(String nombre, String docu) {
        Paciente unPaciente= new Paciente();
        unPaciente.setNombreCompleto(nombre);
        unPaciente.setDocumento(docu);
        return unPaciente;
    }

    private Medicion cargarMedicion(String dni, String time, String oxi, String bpm, String temp, String batLevel){

        return new Medicion(dni, time, oxi,bpm,temp,batLevel);
    }


}

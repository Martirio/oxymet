package com.mriso.oximetro.adapter;

import android.content.Context;
import android.graphics.Typeface;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.mriso.oximetro.R;
import com.mriso.oximetro.fragments.ListaPacientes_fragment;
import com.mriso.oximetro.model.Medicion;
import com.mriso.oximetro.model.Paciente;


import java.io.File;
import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;
import io.realm.Sort;

/**
 * Created by elmar on 18/5/2017.
 */

public class Adapter_Pacientes extends RecyclerView.Adapter implements View.OnClickListener, View.OnLongClickListener {

    private Context context;
    private RealmList<Paciente> listaPacientesOriginales;
    private RealmList<Paciente> listaPacientesFavoritos;
    private View.OnClickListener listener;
    private AdapterView.OnLongClickListener listenerLong;
    private String idPaciente=null;


    public void setLongListener(View.OnLongClickListener unLongListener) {
        this.listenerLong = unLongListener;
    }

    public void setListener(View.OnClickListener listener) {
        this.listener = listener;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public void setListaPacientesOriginales(RealmList<Paciente> listaPacientesOriginales) {
        this.listaPacientesOriginales = listaPacientesOriginales;
    }

    public void addListaPacientesOriginales(RealmList<Paciente> listaPacientesOriginales) {
        this.listaPacientesOriginales.addAll(listaPacientesOriginales);
    }


    public RealmList<Paciente> getListaPacientesOriginales() {
        return listaPacientesOriginales;
    }

    //crear vista y viewholder
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View viewCelda;
        FragmentActivity unaActivity = (FragmentActivity) context;
        FragmentManager fragmentManager = (FragmentManager) unaActivity.getSupportFragmentManager();
        viewCelda = layoutInflater.inflate(R.layout.detalle_celda_mis_pacientes_v2, parent, false);
        viewCelda.setOnClickListener(this);

        return new PacienteViewHolder(viewCelda);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        final Paciente unPaciente = listaPacientesOriginales.get(position);
        PacienteViewHolder auditoriasViewHolder = (PacienteViewHolder) holder;
        assert unPaciente != null;
        auditoriasViewHolder.cargarPaciente(unPaciente);

    }

    @Override
    public int getItemCount() {
        return listaPacientesOriginales.size();
    }


    public void onClick(View view) {
        listener.onClick(view);
    }

    @Override
    public boolean onLongClick(View v) {
        listenerLong.onLongClick(v);
        return true;
    }

    //creo el viewholder que mantiene las referencias
    //de los elementos de la celda

    private class PacienteViewHolder extends RecyclerView.ViewHolder {

        private TextView valorOxigeno;
        private TextView valorTemperatura;
        private TextView valorPulsaciones;
        private TextView nombrePaciente;



        //private TextView textViewTitulo;


        public PacienteViewHolder(View itemView) {
            super(itemView);

            valorOxigeno= itemView.findViewById(R.id.valorOxigeno);
            valorTemperatura= itemView.findViewById(R.id.valorTemperatura);
            valorPulsaciones= itemView.findViewById(R.id.valorPulsaciones);
            nombrePaciente=itemView.findViewById(R.id.textoNombrePaciente);
        }

        public void cargarPaciente(Paciente unPaciente) {


                //busco la ultima medicion
                if (unPaciente.getDocumento()!=null && !unPaciente.getDocumento().isEmpty()) {


                    Realm realm = Realm.getDefaultInstance();
                    Medicion laMedicion= realm.where(Medicion.class)
                            .equalTo("idPacienteRelacionado",unPaciente.getDocumento())
                            .sort("timestamp", Sort.DESCENDING)
                            .findFirst();

                    if (laMedicion!=null){
                        nombrePaciente.setText(unPaciente.getNombreCompleto());

                        String eloxi= laMedicion.getOxigenacion();
                        String latemp= laMedicion.getTemperatura();
                        String laspul= laMedicion.getPulsaciones();

                        int oxi=Integer.parseInt((laMedicion.getOxigenacion()));
                        double temp=(double) (Integer.parseInt(laMedicion.getTemperatura()))/10.0;
                        int bpm=Integer.parseInt((laMedicion.getPulsaciones()));

                        valorOxigeno.setText(eloxi);
                        valorTemperatura.setText(String.valueOf(temp));
                        valorPulsaciones.setText(laspul);

                        if (temp<35.0 || temp>=38.0){
                            valorTemperatura.setBackgroundColor(ContextCompat.getColor(context, R.color.semaRojo));
                        }
                        else if (temp<36.0 || temp>=37.5){
                            valorTemperatura.setBackgroundColor(ContextCompat.getColor(context, R.color.semaAmarillo));
                        }
                        else{
                            valorTemperatura.setBackgroundColor(ContextCompat.getColor(context, R.color.semaVerde));
                        }

                        if (oxi<80 || oxi>=95){
                            valorOxigeno.setBackgroundColor(ContextCompat.getColor(context, R.color.semaRojo));
                        }
                        else if (oxi<85 || oxi>=88){
                            valorOxigeno.setBackgroundColor(ContextCompat.getColor(context, R.color.semaAmarillo));
                        }
                        else {
                            valorOxigeno.setBackgroundColor(ContextCompat.getColor(context, R.color.semaVerde));
                        }

                        if (bpm<60 || bpm>=100){
                            valorPulsaciones.setBackgroundColor(ContextCompat.getColor(context, R.color.semaRojo));
                        }
                        else if (bpm<70 || bpm>=90){
                            valorPulsaciones.setBackgroundColor(ContextCompat.getColor(context, R.color.semaAmarillo));
                        }
                        else {
                            valorPulsaciones.setBackgroundColor(ContextCompat.getColor(context, R.color.semaVerde));
                        }


                    }
                }


        }
    }



}


    // Decodes image and scales it to reduce memory consumption
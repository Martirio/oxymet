package com.mriso.oximetro.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.material.textfield.TextInputLayout;
import com.mriso.oximetro.R;
import com.mriso.oximetro.controller.ControllerDatos;

import io.realm.Realm;

public class Login_Activity extends AppCompatActivity {


    private EditText editMatricula;
    private EditText editPass;
    private TextInputLayout til1;
    private TextInputLayout til2;
    private String contraseña;
    private SharedPreferences config;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_);

//        --INICIAR REALM
        Realm.init(getApplicationContext());
//        INICIAR REALM--

        config = this.getSharedPreferences("prefs", 0);
        boolean loggedIn = config.getBoolean("logged", false);
        if (loggedIn){
           irAMain();
        }
        TextView passwordOlvidada = findViewById(R.id.passwordOlvidada);
        til1 = findViewById(R.id.inputLayout1);
        til2 = findViewById(R.id.inputLayout2);
        Button botonLogin = findViewById(R.id.buttonLogin);
        editMatricula = findViewById(R.id.editTextUsuario);
        editPass = findViewById(R.id.editTextPassword);

        botonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager inputManager = (InputMethodManager) getSystemService(
                        Context.INPUT_METHOD_SERVICE);
                View focusedView = getCurrentFocus();

                if (focusedView != null) {
                    assert inputManager != null;
                    inputManager.hideSoftInputFromWindow(focusedView.getWindowToken(),
                            InputMethodManager.HIDE_NOT_ALWAYS);
                }

                til1.setError(null);
                til2.setError(null);
                int validar = 0;
                if (editMatricula.getText().toString().isEmpty()) {
                    til1.setError(getResources().getString(R.string.ingreseUsuarioValido));
                    validar = validar + 1;
                }

                if (editPass.getText().toString().toLowerCase().isEmpty()) {
                    til2.setError(getResources().getString(R.string.ingreseContraseniaValida));
                    validar = validar + 1;
                }

                if (validar > 0) {
                    return;
                }

                if (validarContrasenia()){
                    irAMain();
                }
                else{
                    new MaterialDialog.Builder(Login_Activity.this)
                            .contentColor(ContextCompat.getColor(Login_Activity.this, R.color.oscuro))
                            .titleColor(ContextCompat.getColor(Login_Activity.this, R.color.turquesaoscuro))
                            .title(R.string.contrasenia_invalida)
                            .cancelable(true)
                            .content(getResources().getString(R.string.texto_contrasenia_incorrecta) )
                            .positiveText(R.string.ok)
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                                }
                            })

                            .show();
                }
            }
        });

        passwordOlvidada.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editMatricula.getText().toString().length()<3) {
                    til1.setError(getResources().getString(R.string.ingreseUsuarioValido));
                    new MaterialDialog.Builder(Login_Activity.this)
                            .contentColor(ContextCompat.getColor(Login_Activity.this, R.color.oscuro))
                            .titleColor(ContextCompat.getColor(Login_Activity.this, R.color.turquesaoscuro))
                            .title(R.string.matricula_ingresar)
                            .cancelable(true)
                            .content(getResources().getString(R.string.texto_matricula_ingresar) )
                            .positiveText(R.string.ok)
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                                }
                            })

                            .show();
                }
                else{
                    new MaterialDialog.Builder(Login_Activity.this)
                            .contentColor(ContextCompat.getColor(Login_Activity.this, R.color.oscuro))
                            .titleColor(ContextCompat.getColor(Login_Activity.this, R.color.turquesaoscuro))
                            .title(R.string.contrasenia_recuperar)
                            .cancelable(true)
                            .content(getResources().getString(R.string.texto_recuperar_contrasenia) )
                            .positiveText(R.string.ok)
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                                }
                            })

                            .show();
                }
            }
        });
    }

    private boolean validarContrasenia() {
        //metodo temporal hasta definir modo de control de usuarios
        if (editMatricula.getText().toString().toLowerCase().equals("1234") && editPass.getText().toString().equals("1234")){

            SharedPreferences.Editor editor = config.edit();
            editor.putBoolean("logged", true);
            editor.commit();

            return true;
        }
        else{
            return false;
        }
    }

    public void irAMain() {

        Intent intent = new Intent(this, Main_Activity.class);
        startActivity(intent);
        Toast.makeText(this, getResources().getString(R.string.bienvenido), Toast.LENGTH_SHORT).show();
        Login_Activity.this.finish();
    }


}

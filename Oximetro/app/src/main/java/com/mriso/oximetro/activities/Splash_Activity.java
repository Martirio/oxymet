package com.mriso.oximetro.activities;

import android.content.Intent;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.mriso.oximetro.BuildConfig;
import com.mriso.oximetro.R;

public class Splash_Activity extends AppCompatActivity {

    private final int SPLASH_DISPLAY_LENGHT = 1500;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        TextView version=findViewById(R.id.versionApp);
        version.setText(BuildConfig.VERSION_NAME);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                Intent mainIntent = new Intent(Splash_Activity.this,Login_Activity.class);
                Splash_Activity.this.startActivity(mainIntent);
                Splash_Activity.this.finish();

            }
        }, SPLASH_DISPLAY_LENGHT);

    }
}

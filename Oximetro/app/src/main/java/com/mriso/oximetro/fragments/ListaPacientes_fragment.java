package com.mriso.oximetro.fragments;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.mriso.oximetro.R;
import com.mriso.oximetro.adapter.Adapter_Pacientes;
import com.mriso.oximetro.model.Paciente;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;


/**
 * A simple {@link Fragment} subclass.
 */
public class ListaPacientes_fragment extends Fragment {

    public ListaPacientes_fragment() {
        // Required empty public constructor
    }

    private Graficable graficable;

    public interface Graficable{
        void GraficarDatosPaciente(Paciente unPacienteGraficable);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_lista_pacientes_v2, container, false);

//      ---BUSCAR EN REALM LISTA DE PACIENTES (TODOS LOS PACIENTES CORRESPONDEN AL MISMO GESTOR)
        Realm realm = Realm.getDefaultInstance();

        RealmResults<Paciente> listaPacientesRealm = realm.where(Paciente.class)
                .findAll();
        RealmList<Paciente> listaPacientes = new RealmList<>();
        listaPacientes.addAll(listaPacientesRealm);
//      BUSCAR EN REALM LISTA DE PACIENTES(TODOS LOS PACIENTES CORRESPONDEN AL MISMO GESTOR)---

//      ---RECYCLERVIEW SETTING
        final RecyclerView recyclerMisPacientes = view.findViewById(R.id.recyclerMisPacientes);
        final Adapter_Pacientes adapterPacientes = new Adapter_Pacientes();
        adapterPacientes.setContext(getContext());
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        recyclerMisPacientes.setLayoutManager(layoutManager);
        adapterPacientes.setListaPacientesOriginales(listaPacientes);
        recyclerMisPacientes.setAdapter(adapterPacientes);
        View.OnClickListener listenerPaciente = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int posicion = recyclerMisPacientes.getChildAdapterPosition(v);
                RealmList<Paciente> listaAuditorias = adapterPacientes.getListaPacientesOriginales();
                Paciente pacienteClickeado = listaAuditorias.get(posicion);
                graficable.GraficarDatosPaciente(pacienteClickeado);
            }
        };
        adapterPacientes.setListener(listenerPaciente);
//      RECYCLERVIEW SETTING---

        return view;
    }

    public static ListaPacientes_fragment createListaPacientesFragment(){
        return new ListaPacientes_fragment();
    }
    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.graficable=(Graficable)context;
    }
}

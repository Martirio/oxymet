package com.mriso.oximetro.model;

import java.util.UUID;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Medicion extends RealmObject {


    private String idPacienteRelacionado;
    private String timestamp;
    private String oxigenacion;
    private String pulsaciones;
    private String temperatura;
    private String bateria;
    @PrimaryKey
    private String id_medicion;

    public Medicion() {
    }

    public Medicion(String idPacienteRelacionado) {
        this.id_medicion = UUID.randomUUID().toString();
        this.idPacienteRelacionado=idPacienteRelacionado;
    }

    public Medicion(String dni, String time, String oxi, String bpm, String temp, String batLevel) {
        this.timestamp=time;
        this.oxigenacion=oxi;
        this.bateria=batLevel;
        this.pulsaciones=bpm;
        this.temperatura=temp;
        this.idPacienteRelacionado=dni;
        this.id_medicion=UUID.randomUUID().toString();
    }

    public String getIdPacienteRelacionado() {
        return idPacienteRelacionado;
    }

    public void setIdPacienteRelacionado(String idPacienteRelacionado) {
        this.idPacienteRelacionado = idPacienteRelacionado;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getOxigenacion() {
        return oxigenacion;
    }

    public void setOxigenacion(String oxigenacion) {
        this.oxigenacion = oxigenacion;
    }

    public String getPulsaciones() {
        return pulsaciones;
    }

    public void setPulsaciones(String pulsaciones) {
        this.pulsaciones = pulsaciones;
    }

    public String getTemperatura() {
        return temperatura;
    }

    public void setTemperatura(String temperatura) {
        this.temperatura = temperatura;
    }

    public String getBateria() {
        return bateria;
    }

    public void setBateria(String bateria) {
        this.bateria = bateria;
    }

    public String getId_medicion() {
        return id_medicion;
    }

}

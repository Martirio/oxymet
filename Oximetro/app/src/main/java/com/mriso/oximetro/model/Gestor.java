package com.mriso.oximetro.model;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Gestor extends RealmObject {

    @PrimaryKey
    private String id_gestor;

    private RealmList<Paciente> listaPacientes;

    public Gestor() {
    }

    public Gestor(String matri) {
        this.id_gestor=matri;
    }

    public String getId_gestor() {
        return id_gestor;
    }

    public void setId_gestor(String id_gestor) {
        this.id_gestor = id_gestor;
    }

    public RealmList<Paciente> getListaPacientes() {
        return listaPacientes;
    }

    public void setListaPacientes(RealmList<Paciente> listaPacientes) {
        this.listaPacientes = listaPacientes;
    }
}

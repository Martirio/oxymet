package com.mriso.oximetro.model;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Paciente extends RealmObject {

    @PrimaryKey
    private String documento;

    private String telefono;
    private String nombreCompleto;
    private RealmList<Medicion>listaMediciones;

    public Paciente() {
    }

    public String getDocumento() {
        return documento;
    }

    public void setDocumento(String documento) {
        this.documento = documento;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getNombreCompleto() {
        return nombreCompleto;
    }

    public void setNombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }

    public RealmList<Medicion> getListaMediciones() {
        return listaMediciones;
    }

    public void setListaMediciones(RealmList<Medicion> listaMediciones) {
        this.listaMediciones = listaMediciones;

    }
}

package com.mriso.oximetro.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import com.mriso.oximetro.R;
import com.mriso.oximetro.model.Medicion;

import java.text.SimpleDateFormat;
import java.util.Date;

import io.realm.RealmList;

/**
 * Created by elmar on 18/5/2017.
 */

public class Adapter_Mediciones extends RecyclerView.Adapter implements View.OnClickListener, View.OnLongClickListener {

    private Context context;
    private RealmList<Medicion> listaMedicionesOriginales;
    private View.OnClickListener listener;
    private AdapterView.OnLongClickListener listenerLong;
    private String idPaciente=null;


    public void setLongListener(View.OnLongClickListener unLongListener) {
        this.listenerLong = unLongListener;
    }

    public void setListener(View.OnClickListener listener) {
        this.listener = listener;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public void setListaMedicionesOriginales(RealmList<Medicion> listaMedicionesOriginales) {
        this.listaMedicionesOriginales = listaMedicionesOriginales;
    }

    public void addListaMedicionesOriginales(RealmList<Medicion> listaMedicionesOriginales) {
        this.listaMedicionesOriginales.addAll(listaMedicionesOriginales);
    }


    public RealmList<Medicion> getListaMedicionesOriginales() {
        return listaMedicionesOriginales;
    }

    //crear vista y viewholder
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View viewCelda;
        FragmentActivity unaActivity = (FragmentActivity) context;
        viewCelda = layoutInflater.inflate(R.layout.detalle_celda_valores, parent, false);
        //viewCelda.setOnClickListener(this);

        return new PacienteViewHolder(viewCelda);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        final Medicion unPaciente = listaMedicionesOriginales.get(position);
        PacienteViewHolder auditoriasViewHolder = (PacienteViewHolder) holder;
        assert unPaciente != null;
        auditoriasViewHolder.cargarPaciente(unPaciente);

    }

    @Override
    public int getItemCount() {
        return listaMedicionesOriginales.size();
    }


    public void onClick(View view) {
        listener.onClick(view);
    }

    @Override
    public boolean onLongClick(View v) {
        listenerLong.onLongClick(v);
        return true;
    }

    //creo el viewholder que mantiene las referencias
    //de los elementos de la celda

    private class PacienteViewHolder extends RecyclerView.ViewHolder {

        private TextView valorOxigeno;
        private TextView valorTemperatura;
        private TextView valorPulsaciones;
        private TextView fecha;



        //private TextView textViewTitulo;


        public PacienteViewHolder(View itemView) {
            super(itemView);
            valorOxigeno= itemView.findViewById(R.id.recycleroxi);
            valorTemperatura= itemView.findViewById(R.id.recyclertemp);
            valorPulsaciones= itemView.findViewById(R.id.recyclerppm);
            fecha=itemView.findViewById(R.id.recyclerFecha);
        }

        public void cargarPaciente(Medicion lame) {


            String eloxi= lame.getOxigenacion();
            String latemp= lame.getTemperatura();
            String laspul= lame.getPulsaciones();
            String fechu = lame.getTimestamp();


            long dv = Long.valueOf(fechu)*1000;
            Date df = new java.util.Date(dv);
            String vv = new SimpleDateFormat("dd/MM/YY_hh:mm").format(df);

            fecha.setText(vv);



            int oxi=Integer.parseInt(eloxi);
            double temp=(double) (Integer.parseInt(latemp))/10.0;
            int bpm=Integer.parseInt(laspul);

            valorOxigeno.setText(eloxi);
            valorTemperatura.setText(String.valueOf(temp));
            valorPulsaciones.setText(laspul);

            if (temp<35.0 || temp>=38.0){
                valorTemperatura.setBackgroundColor(ContextCompat.getColor(context, R.color.semaRojo));
            }
            else if (temp<36.0 || temp>=37.5){
                valorTemperatura.setBackgroundColor(ContextCompat.getColor(context, R.color.semaAmarillo));
            }
            else{
                valorTemperatura.setBackgroundColor(ContextCompat.getColor(context, R.color.semaVerde));
            }

            if (oxi<80 || oxi>=95){
                valorOxigeno.setBackgroundColor(ContextCompat.getColor(context, R.color.semaRojo));
            }
            else if (oxi<85 || oxi>=88){
                valorOxigeno.setBackgroundColor(ContextCompat.getColor(context, R.color.semaAmarillo));
            }
            else {
                valorOxigeno.setBackgroundColor(ContextCompat.getColor(context, R.color.semaVerde));
            }

            if (bpm<60 || bpm>=100){
                valorPulsaciones.setBackgroundColor(ContextCompat.getColor(context, R.color.semaRojo));
            }
            else if (bpm<70 || bpm>=90){
                valorPulsaciones.setBackgroundColor(ContextCompat.getColor(context, R.color.semaAmarillo));
            }
            else {
                valorPulsaciones.setBackgroundColor(ContextCompat.getColor(context, R.color.semaVerde));
            }

        }
    }



}


    // Decodes image and scales it to reduce memory consumption
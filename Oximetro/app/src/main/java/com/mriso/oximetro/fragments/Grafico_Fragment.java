package com.mriso.oximetro.fragments;

import android.graphics.Paint;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.mriso.oximetro.R;
import com.mriso.oximetro.adapter.Adapter_Pacientes;
import com.mriso.oximetro.adapter.Adapter_Mediciones;
import com.mriso.oximetro.model.Medicion;
import com.mriso.oximetro.model.Paciente;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;
import io.realm.Sort;

/**
 * A simple {@link Fragment} subclass.
 */
public class Grafico_Fragment extends Fragment {

    private String idPaciente;
    private Integer contador=0;


    public Grafico_Fragment() {
        // Required empty public constructor
    }
    public static final String IDPACIENTE="IDPACIENTE";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_grafico ,container, false);

        Bundle bundle= getArguments();
        assert bundle != null;
        idPaciente=bundle.getString(IDPACIENTE);

//        TRAER AL PACIENTE
        Realm realm = Realm.getDefaultInstance();
        Paciente pacienteRealm= realm.where(Paciente.class)
                .equalTo("documento",idPaciente)
                .findFirst();

        TextView nombrePersona=view.findViewById(R.id.nombrePersona);
        TextView docuPersona=view.findViewById(R.id.dcouPersona);

        assert pacienteRealm != null;
        nombrePersona.setText(pacienteRealm.getNombreCompleto());
        String docuCompleto= ("DNI "+pacienteRealm.getDocumento());
        docuPersona.setText(docuCompleto);

        RealmResults<Medicion> mediciones = realm.where(Medicion.class)
                .equalTo("idPacienteRelacionado",idPaciente)
                .sort("timestamp", Sort.ASCENDING)
                .limit(10)
                .findAll();

        List<Entry> entriesTemp = new ArrayList<Entry>();
        List<Entry> entriesOxi = new ArrayList<Entry>();
        List<Entry> entriesPuls = new ArrayList<Entry>();
        final List<String> fechas =new ArrayList<>();

        for (Medicion laMed :
                mediciones) {
            entriesOxi.add(new Entry((contador+1)*1f,(Float.parseFloat(laMed.getOxigenacion()))));
            entriesTemp.add(new Entry((contador+1)*1f,(Float.parseFloat(laMed.getTemperatura())/10f)));
            entriesPuls.add(new Entry((contador+1)*1f,(Float.parseFloat(laMed.getPulsaciones()))));

            fechas.add(convertirUnixTimeStamp(laMed.getTimestamp()));
            contador++;
        }

        final LineDataSet dataSetOxi=new LineDataSet(entriesOxi,"Oxigenacion");
        dataSetOxi.setAxisDependency(YAxis.AxisDependency.LEFT);
        dataSetOxi.setColors(getResources().getColor(R.color.oxiLinea));
        dataSetOxi.setCircleColor(getResources().getColor(R.color.oxiLinea));
        dataSetOxi.setLineWidth(2);

        final LineDataSet dataSetTemp=new LineDataSet(entriesTemp,"Temperatura");
       dataSetTemp.setAxisDependency(YAxis.AxisDependency.LEFT);
       dataSetTemp.setColors(getResources().getColor(R.color.tempLinea));
       dataSetTemp.setCircleColor(getResources().getColor(R.color.tempLinea));
       dataSetTemp.setLineWidth(2);

        final LineDataSet dataSetPuls=new LineDataSet(entriesPuls,"Pulso");
        dataSetPuls.setAxisDependency(YAxis.AxisDependency.LEFT);
        dataSetPuls.setColors(getResources().getColor(R.color.pulsoLinea));
        dataSetPuls.setCircleColor(getResources().getColor(R.color.pulsoLinea));
        dataSetPuls.setLineWidth(2);



        ValueFormatter valueFormatter=new ValueFormatter() {
            @Override
            public String getAxisLabel(float value, AxisBase axis) {
                return fechas.get((int)value-1);
            }
        };

        final LineChart chart = view.findViewById(R.id.chart);
        chart.getAxisLeft().setTextSize(12);
        chart.getAxisLeft().setAxisLineWidth(2);
        chart.getAxisRight().setEnabled(false);
        chart.getXAxis().setAxisLineWidth(2);
        chart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        chart.getXAxis().setGranularity(1f);
        chart.getXAxis().setValueFormatter(valueFormatter);
        chart.getDescription().setEnabled(false);
        chart.getXAxis().setLabelRotationAngle(-45);

        List<ILineDataSet> dataSets= new ArrayList<>();
        dataSets.add(dataSetOxi);
        LineData laData= new LineData(dataSets);

        Legend l = chart.getLegend();
        l.setEnabled(true);
        l.setFormSize(10f); // set the size of the legend forms/shapes
        l.setForm(Legend.LegendForm.CIRCLE); // set what type of form/shape should be used
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.CENTER);
        l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        l.setDrawInside(false);
        chart.setData(laData);
        chart.invalidate();

        Button btn_oxi = view.findViewById(R.id.botonOxi);
        btn_oxi.setPaintFlags(btn_oxi.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        Button btn_temp = view.findViewById(R.id.botonTemp);
        btn_temp.setPaintFlags(btn_temp.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        Button btn_puls = view.findViewById(R.id.botonPuls);
        btn_puls.setPaintFlags(btn_puls.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        btn_oxi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<ILineDataSet> dataSets_O= new ArrayList<>();
                dataSets_O.add(dataSetOxi);
                LineData laData_O= new LineData(dataSets_O);
                chart.setData(laData_O);
                chart.invalidate();
            }
        });

        btn_temp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<ILineDataSet> dataSets_T= new ArrayList<>();
                dataSets_T.add(dataSetTemp);
                LineData laData_T= new LineData(dataSets_T);
                chart.setData(laData_T);
                chart.invalidate();
            }
        });

        btn_puls.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<ILineDataSet> dataSets_P= new ArrayList<>();
                dataSets_P.add(dataSetPuls);
                LineData laData_P= new LineData(dataSets_P);
                chart.setData(laData_P);
                chart.invalidate();
            }
        });

//        CARGAR TABLA DE DATOS
        RealmList<Medicion> listaMedicionesOriginales = new RealmList<>();
        listaMedicionesOriginales.addAll(mediciones.sort("timestamp",Sort.DESCENDING));
        final RecyclerView recyclerMisMediciones = view.findViewById(R.id.recyclerTabla);
        final Adapter_Mediciones adapterPacientes = new Adapter_Mediciones();
        adapterPacientes.setContext(getContext());
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        recyclerMisMediciones.setLayoutManager(layoutManager);
        adapterPacientes.setListaMedicionesOriginales(listaMedicionesOriginales);
        recyclerMisMediciones.setAdapter(adapterPacientes);

        return view;
    }

    private String convertirUnixTimeStamp(String timestamp) {

        long dv = Long.valueOf(timestamp)*1000;
        Date df = new java.util.Date(dv);
        String vv = new SimpleDateFormat("HH:mm").format(df);

        return vv;
    }
}
